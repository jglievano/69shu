package book

import (
	"bytes"
	"io"

	"golang.org/x/net/html"
)

func renderNode(node *html.Node) string {
	var buf bytes.Buffer
	w := io.Writer(&buf)
	html.Render(w, node)
	return buf.String()
}

func getHref(node *html.Node) string {
	for _, attr := range node.Attr {
		if attr.Key == "href" {
			return attr.Val
		}
	}
	return ""
}

func nodeIsOfClass(node *html.Node, className string) bool {
	for _, attr := range node.Attr {
		if attr.Key == "class" {
			if attr.Val == className {
				return true
			}
		}
	}
	return false
}

// Gets FIRST child in parent node with class.
func getChildOfClass(parent *html.Node, tag string, className string) *html.Node {
	for c := parent.FirstChild; c != nil; c = c.NextSibling {
		if c.Type == html.ElementNode && c.Data == tag {
			if nodeIsOfClass(c, className) {
				return c
			}
		}
	}
	return nil
}

func getChildOfClassRecursively(node *html.Node, tag string, className string) *html.Node {
	if node.Type == html.ElementNode && node.Data == tag {
		if nodeIsOfClass(node, className) {
			return node
		}
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		content := getChildOfClassRecursively(c, tag, className)
		if content != nil {
			return content
		}
	}
	return nil
}

// Gets FIRST child in parent node with tag.
func getChildWithTag(parent *html.Node, tag string) *html.Node {
	for c := parent.FirstChild; c != nil; c = c.NextSibling {
		if c.Type == html.ElementNode && c.Data == tag {
			return c
		}
	}
	return nil
}

func getChildTextString(parent *html.Node) string {
	for text := parent.FirstChild; text != nil; text = text.NextSibling {
		if text.Type == html.TextNode {
			return text.Data
		}
	}
	return ""
}

func getChapterListNode(node *html.Node) *html.Node {
	if node.Type == html.ElementNode && node.Data == "div" {
		if nodeIsOfClass(node, "mu_contain") {
			// Check if "mu_h1" class element exists, meaning is not the complete chapter list.
			if getChildOfClass(node, "div", "mu_h1") == nil {
				return getChildOfClass(node, "ul", "mulu_list")
			}
		}
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		list := getChapterListNode(c)
		if list != nil {
			return list
		}
	}
	return nil
}

func getChapterContentNode(node *html.Node) *html.Node {
	if node.Type == html.ElementNode && node.Data == "div" {
		if nodeIsOfClass(node, "yd_text2") {
			return node
		}
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		content := getChapterContentNode(c)
		if content != nil {
			return content
		}
	}
	return nil
}
