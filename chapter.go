package book

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"time"

	"golang.org/x/net/html"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

// Chapter represents a chapter to be downloaded.
type Chapter struct {
	Index int
	Name  string
	URL   string
	Path  string
}

func (chapter *Chapter) removeCrap(node *html.Node) {
	for child := getChildWithTag(node, "div"); child != nil; child = getChildWithTag(node, "div") {
		node.RemoveChild(child)
	}
	for child := getChildWithTag(node, "script"); child != nil; child = getChildWithTag(node, "script") {
		node.RemoveChild(child)
	}
}

// Download downloads and parse chapter URLs.
func (chapter *Chapter) Download(tmpDir string, chFinished chan bool, chErrs chan error) {
	defer func() {
		chFinished <- true
	}()

	var resp *http.Response
	var doc *html.Node
	var err error

	maxRetries := 5
	parseRetries := 0
	for true {
		downloadRetries := 0
		for true {
			resp, err = http.Get(chapter.URL)
			if err == nil {
				defer resp.Body.Close()
				break
			}
			if err != nil && downloadRetries >= maxRetries {
				chErrs <- fmt.Errorf("Failed downloading chapter %d: %v", chapter.Index, err)
				return
			} else if err != nil {
				time.Sleep(10 * time.Second)
				downloadRetries++
			}
		}

		r := transform.NewReader(resp.Body, simplifiedchinese.GBK.NewDecoder())

		doc, err = html.Parse(r)
		if err == nil {
			break
		}
		if err != nil && parseRetries >= maxRetries {
			chErrs <- fmt.Errorf("Failed to parse chapter %d: %v", chapter.Index, err)
			break
		} else if err != nil {
			time.Sleep(10 * time.Second)
			parseRetries++
		}
	}

	chapterContent := getChapterContentNode(doc)
	chapter.removeCrap(chapterContent)
	content := renderNode(chapterContent)

	content = regexp.MustCompile("<!--(.|\n)*?-->").ReplaceAllString(content, "")
	content = regexp.MustCompile("<div.*>").ReplaceAllString(content, "")
	content = regexp.MustCompile("</div>").ReplaceAllString(content, "")
	content = regexp.MustCompile(`^[\s\p{Zs}]+|[\s\p{Zs}]+$`).ReplaceAllString(content, "")
	content = regexp.MustCompile(`[\s\p{Zs}]{2,}`).ReplaceAllString(content, "")
	content = regexp.MustCompile(`(^|\n)<br/>`).ReplaceAllString(content, "\n")
	content = fmt.Sprintf("<h2>%s</h2><br/>\n%s<br/>\n", chapter.Name, content)

	filename := filepath.Join(tmpDir, fmt.Sprintf("%d.html", chapter.Index))
	f, err := os.Create(filename)
	if err != nil {
		chErrs <- fmt.Errorf("Failed to create %s: %v", filename, err)
		return
	}
	defer f.Close()
	_, err = f.WriteString(content)
	if err != nil {
		chErrs <- fmt.Errorf("Failed to write chapter %d: %v", chapter.Index, err)
	}
	chapter.Path = filename
}
