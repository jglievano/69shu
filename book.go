package book

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"

	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"

	"golang.org/x/net/html"
)

const (
	baseURL = "http://www.69shu.com/"
)

// Book represents a book to be downloaded.
type Book struct {
	ID       int
	Name     string
	Authors  string
	cover    string
	URL      string
	Path     string
	tmpDir   string
	Chapters []*Chapter
}

func NewBook(id int) (*Book, error) {
	var book = &Book{
		ID:  id,
		URL: fmt.Sprintf("%s%d/", baseURL, id),
	}

	resp, err := http.Get(book.URL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	r := transform.NewReader(resp.Body, simplifiedchinese.GBK.NewDecoder())
	doc, err := html.Parse(r)
	if err != nil {
		return nil, err
	}

	book.cover = fmt.Sprintf("http://www.69shu.com/files/article/image/%d/%d/%ds.jpg", thousandths(book.ID), book.ID, book.ID)
	book.assignTmpDir()
	book.assignAuthors(doc)
	book.assignTitle(doc)
	book.assignChapters(doc)

	log.Printf("Completed processing structure for \"%s\"", book.Name)

	if err = book.downloadChapters(book.tmpDir); err != nil {
		return nil, err
	}
	return book, nil
}

func (book *Book) Description() {
	fmt.Printf("\n\n--- Book ---\n%s\nID: %d\nAuthors: %s\n# of Chapters: %d\n---\n\n",
		book.Name, book.ID, book.Authors, len(book.Chapters))
}

func (book *Book) assignChapters(node *html.Node) {
	list := getChapterListNode(node)
	chapters := []*Chapter{}
	count := 1
	if list != nil {
		for li := list.FirstChild; li != nil; li = li.NextSibling {
			if li.Type == html.ElementNode {
				link := getChildWithTag(li, "a")
				chapters = append(chapters, &Chapter{
					Index: count,
					Name:  getChildTextString(link),
					URL:   getHref(link),
				})
				count++
			}
		}
	}

	book.Chapters = chapters
}

func (book *Book) assignAuthors(node *html.Node) {
	outer := getChildOfClassRecursively(node, "div", "mu_beizhu")
	if outer == nil {
		book.Authors = "NN"
		return
	}
	inner := getChildOfClass(outer, "div", "mu_beizhu")
	if inner == nil {
		inner = outer
	}
	link := getChildWithTag(inner, "a")
	if link == nil {
		book.Authors = "NN"
		return
	}
	book.Authors = getChildTextString(link)
}

func (book *Book) assignTitle(node *html.Node) {
	header := getChildOfClassRecursively(node, "div", "mu_h1")
	if header == nil {
		book.Name = "Unknown"
		return
	}
	title := getChildWithTag(header, "h1")
	if title == nil {
		book.Name = "Unknown"
		return
	}
	book.Name = getChildTextString(title)
}

func (book *Book) assignTmpDir() error {
	tmpDir, err := ioutil.TempDir("", strconv.Itoa(book.ID))
	if err != nil {
		return err
	}
	book.tmpDir = tmpDir
	return nil
}

func (book *Book) downloadChapters(tmpDir string) error {
	chFinished := make(chan bool)
	chErrs := make(chan error)

	chapterCount := len(book.Chapters)
	capacity := 300
	hasErrors := false
	chapter := 0
	for downloads := 0; downloads < chapterCount; {
		if capacity > 0 && chapter < chapterCount {
			go book.Chapters[chapter].Download(book.tmpDir, chFinished, chErrs)
			chapter++
			capacity--
			continue
		}
		select {
		case err := <-chErrs:
			log.Printf("%v", err)
			hasErrors = true
		case <-chFinished:
			downloads++
			capacity++
			fmt.Printf("\rDownloaded chapter %d out of %d", downloads, chapterCount)
		}
	}

	if hasErrors {
		return fmt.Errorf("Errors found processing chapters")
	}

	log.Println("\nDownload completed!")
	return nil
}

func thousandths(num int) int {
	return num / 1000
}

func (book *Book) exportHTML() error {
	filename := filepath.Join(book.tmpDir, "complete.html")
	out, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer out.Close()

	out.WriteString("<html lang=\"zh-CN\">\n<head>")
	out.WriteString("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=gbk\">\n")
	out.WriteString(fmt.Sprintf("<title>%s</title>\n", book.Name))
	out.WriteString("</head>\n<body>")
	chapterCount := len(book.Chapters)
	for c := 0; c < chapterCount; c++ {
		chapter := book.Chapters[c]
		in, err := ioutil.ReadFile(chapter.Path)
		if err != nil {
			return fmt.Errorf("Chapter %d w/path: %s -> %v", c, chapter.Path, err)
		}
		out.Write(in)
	}
	out.WriteString("</body>\n</html>\n")
	book.Path = filename
	return nil
}

// SendTo sends MOBI book to specified email.
func (book *Book) SendTo(email string) error {
	filename := filepath.Join(book.tmpDir, "complete.mobi")
	cmd := exec.Command("echo", "\"\"", "|", "mutt", "-a", filename, "-s",
		"\"Book\"", "--", email)
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

// ConvertToMOBI converts book HTML to MOBI.
func (book *Book) ExportMOBI() error {
	if err := book.exportHTML(); err != nil {
		return err
	}

	filename := filepath.Join(book.tmpDir, "complete.mobi")

	// TODO: darwin OS won't find ebook-convert.
	cmd := exec.Command("ebook-convert", book.Path, filename, "--level1-toc", "//h:h2", "--output-profile",
		"kindle_voyage", "--language", "zh-CN", "--max-toc-links", "3000", "--title", book.Name,
		"--authors", book.Authors, "--cover", book.cover)
	var stdoutBuf bytes.Buffer
	stdoutIn, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	var errStdout error
	stdout := io.MultiWriter(os.Stdout, &stdoutBuf)

	if err = cmd.Start(); err != nil {
		return err
	}
	go func() {
		_, errStdout = io.Copy(stdout, stdoutIn)
	}()

	if err = cmd.Wait(); err != nil {
		return err
	}
	if errStdout != nil {
		return errStdout
	}
	outStr := string(stdoutBuf.Bytes())
	fmt.Printf("\n%s\n", outStr)

	return nil
}
